import "./App.css";
import axios from "axios";
import { useEffect, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  CircularProgress,
  Dialog,
  List,
  ListItem,
  TextField,
  Typography,
} from "@material-ui/core";
import MaterialTable from "material-table";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  PieChart,
  Pie,
  Tooltip,
} from "recharts";

const REMOTE_URL =
  process.env.NODE_ENV === "production"
    ? "https://matinnikookar.vercel.app/"
    : "/";

function App() {
  const [movies, setMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [year, setYear] = useState(1960);
  const [formData, setFormData] = useState({ year: "", cast: "" });
  const [cast, setCast] = useState("");
  const [firstChartData, setFirstChartData] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [secondChartData, setSecondChartData] = useState([]);
  const [selectedMovie, setSelectedMovie] = useState();
  const [render, setRender] = useState(false);
  const [genres, setGenres] = useState([]);

  useEffect(() => {
    var obj = {};
    var genreArray = [];
    var genreCount = {};
    if (cast) obj = { cast };
    if (year) obj = { year };
    if (year && cast) obj = { year, cast };

    if (!(obj && Object.keys(obj).length === 0 && obj.constructor === Object)) {
      setIsLoading(true);
      axios
        .post(REMOTE_URL, {
          db: "sample_mflix",
          collection: "movies",
          query: obj,
          sort: { method: "des", parameter: "released" },
          loadComments: false,
        })
        .then((response) => {
          var firstChartDataArray = [];
          var secondChartDataArray = [];
          setMovies(response.data.cursor);
          response.data?.cursor?.forEach((item) => {
            firstChartDataArray.push({
              x: item.runtime,
              y: item.imdb.rating,
            });

            secondChartDataArray.push({
              x: item.awards.wins,
              y: item.imdb.rating,
            });

            item?.genres?.forEach((genre) => genreArray.push(genre));
          });

          for (var i = 0; i < genreArray.length; i++) {
            var num = genreArray[i];
            genreCount[num] = genreCount[num] ? genreCount[num] + 1 : 1;
          }
          const genreToSubmit = [];
          for (const [k, v] of Object.entries(genreCount)) {
            genreToSubmit.push({ name: k, value: v });
          }
          setGenres(genreToSubmit);
          setFirstChartData(firstChartDataArray);
          setSecondChartData(secondChartDataArray);
          setIsLoading(false);
        });
    }
  }, [year, cast, render]);

  const columns = [
    {
      title: "Title",
      field: "title",
      render: (rowData) => (
        <Typography
          onClick={() => {
            setSelectedMovie(rowData);
            setDialogOpen(true);
          }}
          color="secondary"
          style={{ textDecoration: "underline", cursor: "pointer" }}
        >
          {rowData.title}
        </Typography>
      ),
    },
    { title: "Type", field: "type" },
    { title: "Year", editable: "never", field: "year" },
    { title: "IMDB Rating", editable: "never", field: "imdb.rating" },
    { title: "Duration", field: "runtime" },
    {
      title: "Genres",
      field: "genres",
      editable: "never",
      render: (rowData) => (
        <Typography variant="subtitle2">
          {rowData?.genres?.join(", ")}
        </Typography>
      ),
    },
  ];

  function handleSubmit(event) {
    event.preventDefault();
    const { year, cast } = formData;
    setYear(year);
    setCast(cast);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Typography
          style={{ marginBottom: "1rem" }}
          variant="h3"
          component="h1"
        >
          Big Data Final Project
        </Typography>

        <Typography variant="caption">
          By Matin Nikookar, Muhammad Muneeb Ahmad, Tim Kobiolka, Kyulim Kim and
          Arsalan Shahid Baig
        </Typography>

        <div className="table">
          <form onSubmit={handleSubmit}>
            <TextField
              classes={{ root: "input" }}
              value={formData.year}
              onChange={(e) =>
                setFormData({ ...formData, year: parseInt(e.target.value) })
              }
              variant="filled"
              size="small"
              helperText="Between 1915 to 2013"
              color="secondary"
              type="number"
              label="Release Year"
            />
            <TextField
              classes={{ root: "input" }}
              value={formData.cast}
              onChange={(e) =>
                setFormData({ ...formData, cast: e.target.value })
              }
              variant="filled"
              size="small"
              helperText="Search for a cast member"
              color="secondary"
              type="text"
              InputProps={{ min: 1915, max: 2020 }}
              label="Actor/Actress"
            />

            <Button
              type="submit"
              variant="contained"
              color="secondary"
              disabled={isLoading}
            >
              Submit
            </Button>
          </form>
          {isLoading && <CircularProgress color="secondary" />}
          {!isLoading && (
            <MaterialTable
              editable={{
                onRowUpdate: async (newData) => {
                  setIsLoading(true);
                  await axios.put(REMOTE_URL, {
                    db: "sample_mflix",
                    collection: "movies",
                    query: { _id: newData._id.$oid },
                    newValues: {
                      title: newData.title,
                      type: newData.type,
                      runtime: newData.runtime,
                    },
                  });
                  setRender((state) => !state);
                  setIsLoading(false);
                },
                onRowDelete: async (data) => {
                  setIsLoading(true);
                  await axios.delete(
                    `${REMOTE_URL}?db=sample_mflix&collection=movies&id=${data._id.$oid}`
                  );
                  setRender((state) => !state);
                  setIsLoading(false);
                },
              }}
              title={"M_Flix Database Instance"}
              data={movies}
              columns={columns}
            />
          )}
        </div>
        {!isLoading && (
          <>
            <Typography variant="h4">
              Duration to Rating Ratio of Movies{" "}
              {`${year ? `from Year ${year}` : ""} ${cast ? `by ${cast}` : ""}`}
            </Typography>
            <ScatterChart
              width={700}
              height={400}
              margin={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
              }}
            >
              <CartesianGrid />
              <XAxis
                domain={[0, "dataMax"]}
                type="number"
                dataKey="x"
                name="Duration"
                unit=" mins"
                accentHeight={10}
              />
              <YAxis
                domain={[0, 10]}
                type="number"
                dataKey="y"
                name="Rating"
                unit=" /10"
              />
              <Tooltip cursor={{ strokeDasharray: "3 3" }} />
              <Scatter name="A school" data={firstChartData} fill="#c51162" />
            </ScatterChart>

            <Typography variant="h4">
              Rating to Award Winning Ratio of Movies{" "}
              {`${year ? `from Year ${year}` : ""} ${cast ? `by ${cast}` : ""}`}
            </Typography>
            <ScatterChart
              width={700}
              height={400}
              margin={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
              }}
            >
              <CartesianGrid />
              <XAxis
                domain={[0, "dataMax"]}
                type="number"
                dataKey="x"
                name="Awards Won"
                unit="#"
                accentHeight={10}
              />
              <YAxis
                domain={[0, 10]}
                type="number"
                dataKey="y"
                name="Rating"
                unit=" /10"
              />
              <Tooltip cursor={{ strokeDasharray: "3 3" }} />
              <Scatter name="A school" data={secondChartData} fill="#4bade3" />
            </ScatterChart>

            <Box
              display="flex"
              flexDirection="row"
              flexWrap="wrap"
              alignItems="center"
              justifyContent="center"
            >
              <PieChart width={300} height={300}>
                <Pie
                  dataKey="value"
                  startAngle={0}
                  endAngle={360}
                  data={genres}
                  cx="50%"
                  cy="50%"
                  outerRadius={80}
                  fill="#8884d8"
                  label
                />
              </PieChart>
              <Typography variant="h4">
                Category List of Movies{" "}
                {`${year ? `from Year ${year}` : ""} ${
                  cast ? `by ${cast}` : ""
                }`}
              </Typography>
              <List
                style={{ display: "flex", flexFlow: "row wrap", width: "100%" }}
              >
                {genres.map((genre) => (
                  <ListItem
                    key={genre.name}
                    style={{ width: "20%", textAlign: "center" }}
                  >
                    {genre.name}: {genre.value}
                  </ListItem>
                ))}
              </List>
            </Box>

            {dialogOpen && (
              <Dialog
                fullWidth
                title={`Comments for ${selectedMovie?.title}`}
                open={dialogOpen}
              >
                <Card>
                  <CardContent>
                    <Typography variant="body1">
                      Plot: {selectedMovie?.fullplot}
                    </Typography>

                    <Typography style={{ fontWeight: "bold" }} variant="body1">
                      Cast: {selectedMovie?.cast?.join(", ")}
                    </Typography>
                  </CardContent>
                </Card>
                <Button
                  color="secondary"
                  variant="outlined"
                  onClick={() => {
                    setSelectedMovie(false);
                    setDialogOpen(false);
                  }}
                >
                  Close
                </Button>
              </Dialog>
            )}
          </>
        )}
      </header>
    </div>
  );
}

export default App;
